#include "GameSettingsScreen.h"

#include "GameSettings.h"

//  cael
//  defined in main.cpp
extern GameSettings gameSettings;

GameSettingsScreen::GameSettingsScreen() {

	m_pTexture = nullptr;

	//SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);
	Show();
}


//  cael
void OnExitToMain(MenuScreen* pScreen) {
	pScreen->Exit();
}

void SetPlayerShipMovementSpeed(float shipSpeed) {
	gameSettings.SetPlayerShipSpeed(shipSpeed);
}

//  cael
void OnSetPlayerShipMovementSpeed300(MenuScreen* pScreen) {
	SetPlayerShipMovementSpeed(300);
	pScreen->Exit();
}
void OnSetPlayerShipMovementSpeed600(MenuScreen* pScreen) {
	SetPlayerShipMovementSpeed(600);
	pScreen->Exit();
}
void OnSetPlayerShipMovementSpeed1200(MenuScreen* pScreen) {
	SetPlayerShipMovementSpeed(1200);
	pScreen->Exit();
}



void GameSettingsScreen::LoadContent(ResourceManager* pResourceManager) {
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\SettingsScreen.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 4;
	MenuItem* pItem;
	Font::SetLoadSize(20, true);
	Font* pFont = pResourceManager->Load<Font>("Fonts\\arial.ttf");

	SetDisplayCount(COUNT);

	enum Items {
		PLAYER_SHIP_MOVEMENT_SPEED300,
		PLAYER_SHIP_MOVEMENT_SPEED600,
		PLAYER_SHIP_MOVEMENT_SPEED1200,
		EXIT_TO_MAIN
	};
	std::string text[COUNT] = {
		"Player Ship Movement Speed = 300",
		"Player Ship Movement Speed = 600",
		"Player Ship Movement Speed = 1200",

		"Return to Main menu"
	};

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	//	GetMenuItem(SHIP_MOVEMENT_SPEED)->SetSelectCallback(OnSettingsSelect);
	//	GetMenuItem(EXIT_TO_MAIN)->SetSelectCallback(OnQuitSelect);

	GetMenuItem(PLAYER_SHIP_MOVEMENT_SPEED300)->SetSelectCallback(OnSetPlayerShipMovementSpeed300);
	GetMenuItem(PLAYER_SHIP_MOVEMENT_SPEED600)->SetSelectCallback(OnSetPlayerShipMovementSpeed600);
	GetMenuItem(PLAYER_SHIP_MOVEMENT_SPEED1200)->SetSelectCallback(OnSetPlayerShipMovementSpeed1200);
	GetMenuItem(EXIT_TO_MAIN)->SetSelectCallback(OnExitToMain);

}



void GameSettingsScreen::Update(const GameTime* pGameTime) {
	MenuItem* pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::White);
		else pItem->SetColor(Color::Blue);
	}

	MenuScreen::Update(pGameTime);
}

void GameSettingsScreen::Draw(SpriteBatch* pSpriteBatch) {
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}